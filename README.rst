========
Overview
========

What is D-Link Default KeyGenerator:


D-Link KeyGenerator is a simple,lightweight freeware application written in Java , 
witch main purpose is to provide default passwords for D-link routers. 


=============
How to use it
=============

The User Interface is pretty simple,just fill in the routers Mac-Address and it will instantly generate 
a key to access the network.
Please note that the routers Mac-Address is not the same as the SSID,so in order to get it simply go to the 
terminal and enter the following command : 'iwlist [interface] scan' if you are using Linux or use a detection software
like Netstumbler.
After that enter the mac address for example : 00:22:B0:F0:29:AA and it will output the wireless key. 

=============
Requirements
=============

-This program can run on any platform (Operating System) as long as you have java runtime enviroment installed.

To run the project after extracting the .zip file, go to the extracted folder and double-click the "DlinkKeyGen.jar" if you have the java runtime enviroment installed (jre 1.6 or later).
If you don't, then go to http://javadl.sun.com/webapps/download/AutoDL?BundleId=20287 , download it and install it.


To run the project from the command line, go to the D-link Default KeyGen folder and
type the following:

java -jar "DlinkKeyGen.jar" 

To distribute this project, zip up the main folder (including the lib folder)
and distribute the ZIP file.

=====================================

D-Link Default KeyGenerator

Author/Developer : Nuno Khan

Version 1.0

2010

=====================================

=====================================

Technical Support
	nunok7@gmail.com

=====================================
