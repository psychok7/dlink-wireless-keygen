package Common;

import java.io.IOException;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Iwlistscan {
  
  protected String iwlist_interface;
  protected List<String> commands;
  public StringBuilder stdout,stderr;
  // can run basic ls or ps commands
  // can run command pipelines
  // can run sudo command if you know the password is correct
  public Iwlistscan(String iwlist_interface) throws IOException, InterruptedException {

    this.iwlist_interface=iwlist_interface;
    // build the system command we want to run
    commands = new ArrayList<String>();
    this.commands.add("/bin/sh");
    this.commands.add("-c");
    this.commands.add("iwlist "+this.iwlist_interface+" scan");
    start_iwlistscan();
  }

    public void start_iwlistscan(){
        // execute the command
    SystemCommandExecutor commandExecutor = new SystemCommandExecutor(commands);
        try {
            int result = commandExecutor.executeCommand();
        } catch (IOException ex) {
            Logger.getLogger(Iwlistscan.class.getName()).log(Level.SEVERE, null, ex);
        } catch (InterruptedException ex) {
            Logger.getLogger(Iwlistscan.class.getName()).log(Level.SEVERE, null, ex);
        }

    // get the stdout and stderr from the command that was run
    this.stdout = commandExecutor.getStandardOutputFromCommand();
    this.stderr = commandExecutor.getStandardErrorFromCommand();

    // print the stdout and stderr
//    System.out.println("The numeric result of the command was: " + result);
//    System.out.println("STDOUT:");
//    System.out.println(stdout);
//    System.out.println("STDERR:");
//    System.out.println(stderr);

    }

}
