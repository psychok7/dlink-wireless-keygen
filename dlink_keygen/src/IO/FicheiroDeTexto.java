package IO;

import java.io.*;
public class FicheiroDeTexto {
   private BufferedReader fR;
   private BufferedWriter fW;

   
   public FicheiroDeTexto(){ 
       
   }

public void abreLeitura(String nomeDoFicheiro) throws IOException {
   fR = new BufferedReader(new FileReader(nomeDoFicheiro));
}
public void abreEscrita(String nomeDoFicheiro) throws IOException {
   fW = new BufferedWriter(new FileWriter(nomeDoFicheiro));
}

//Método para ler uma linha do ficheiro
//Devolve a linha lida
public String lerLinha() throws IOException {
   return fR.readLine();
                      
}
    
//Método para ler um número do ficheiro
//Devolve o número lido
public int[] lerNumeroInt() throws IOException {
    int[] result = new int[2];
    String st = fR.readLine();
    if (st != null) {
               result[0] = 0;
               result[1] = Integer.parseInt(st);
    } else {
               result[0] = -1;}
    return result;
}

//Método para escrever uma linha no ficheiro
//Recebe a linha a escrever
public void escreverLinha(String linha) throws IOException {
   fW.write(linha,0,linha.length());
   fW.newLine();
}

//Método para escrever um número inteiro no ficheiro
//Recebe o número a escrever
public void escreverNumero(int num) throws IOException {
   String st = "";
   st = st.valueOf(num);
   escreverLinha(st);
}

//Método para fechar um ficheiro aberto em modo leitura
public void fechaLeitura() throws IOException {
   fR.close();
}
//Método para fechar um ficheiro aberto em modo escrita
public void fechaEscrita() throws IOException {
   fW.close();
}

    
    }
